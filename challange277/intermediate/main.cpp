#include<iostream>
#include<fstream>
#include<string>
#include<queue>
int main(){
    //This will hold the lines as they are read in
    std::string line;
    //Store the lines in a queue
    std::queue<std::string> weightQueue;
    //File for input
    std::ifstream inputFile;

    inputFile.open("coins.txt");
    while (std::getline(inputFile, line)){
        weightQueue.push(line);
    }   
    inputFile.close();
    std::string queueLine;
    //Print each line that was added to the queue
    while(weightQueue.size() != 0)
    {
        queueLine = weightQueue.front();
        weightQueue.pop();
        std::cout << queueLine << std::endl;
    }
    return 0;
}
